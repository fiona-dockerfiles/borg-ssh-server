FROM ubuntu:latest

RUN DEBIAN_FRONTEND=noninteractive &&\
    apt-get update &&\
    apt-get -y full-upgrade &&\
    apt-get -y install \
        borgbackup \
        ssh &&\
    mkdir /var/run/sshd

VOLUME ["/data"]
EXPOSE 22/tcp

ENTRYPOINT /usr/sbin/sshd -D -e
